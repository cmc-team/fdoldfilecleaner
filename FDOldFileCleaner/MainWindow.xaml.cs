﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Media.Animation;

namespace FDOldFileCleaner
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(string lpAppName, byte[] lpszReturnBuffer, int nSize, string lpFileName);

        public static string GetIniFileString(string section, string key, string def, string filePath)
        {
            StringBuilder temp = new StringBuilder(10240);
            GetPrivateProfileString(section, key, def, temp, 10240, filePath);
            return temp.ToString();
        }

        private List<string> GetKeys(string section, string filePath)
        {
            try
            {
                byte[] buffer = new byte[10240];

                GetPrivateProfileSection(section, buffer, 10240, filePath);
                String[] tmp = Encoding.ASCII.GetString(buffer).Trim('\0').Split('\0');

                List<string> result = new List<string>();

                foreach (String entry in tmp)
                {
                    result.Add(entry.Substring(0, entry.IndexOf("=")));
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }

            
        }

        public string strINIPath = string.Empty;
        public string strINIUpdate = string.Empty;
        public string strTargetFolderPath = string.Empty;
        public string str_useLastIpsw = string.Empty;
        public List<string> listKey_lastipsw = new List<string>();
        public List<string> listKey_lastdmg = new List<string>();
        public List<string> listKey_ipsw = new List<string>();
        public List<string> listKey_dmg = new List<string>();
        public List<string> listAllValue = new List<string>();
        public List<string> listAllLocalFiles = new List<string>();
        bool bFirstCheck = true;

        StringBuilder incomingdata = new StringBuilder();

        public MainWindow()
        {
            InitializeComponent();            
            btnDelete.Content = LoadResourceString.Instance.GetStrByResource("delete_all_old_file");
            tblockItemTitle.Text = LoadResourceString.Instance.GetStrByResource("old_file");

            strINIPath = System.IO.Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "IOSFile.ini");
            strINIUpdate = System.IO.Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "iOSFileSync.exe");
            strTargetFolderPath = Environment.GetEnvironmentVariable("APSTHOME") + @"\PST\Apple\Common\dmg\";
            try
            {
                if (File.Exists(strINIUpdate))
                {

                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = strINIUpdate;
                    p.StartInfo.Arguments = "-queryIOSFile";
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                    p.Start();
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();

                    if (p.WaitForExit(40000))
                    {
                        if (p.ExitCode == 1)
                        {
                            MessageBox.Show("\"iOSFileSync.exe\" Error. ", LoadResourceString.Instance.GetStrByResource("remind"), MessageBoxButton.OK, MessageBoxImage.Asterisk);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("\"iOSFileSync.exe\" Timeout Error. ", LoadResourceString.Instance.GetStrByResource("remind"), MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    
                }
                else
                {
                    MessageBox.Show("System not found \"iOSFileSync.exe\". ", LoadResourceString.Instance.GetStrByResource("remind"), MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    this.Close();
                }


                if (File.Exists(strINIPath))
                {

                    str_useLastIpsw = GetIniFileString("config", "useLastIpsw", "", strINIPath);

                    if (str_useLastIpsw.ToLower() == "true")
                    {
                        listKey_lastipsw.AddRange(GetKeys("last_ipsw", strINIPath));
                        foreach (var item in listKey_lastipsw)
                        {
                            listAllValue.Add(GetIniFileString("last_ipsw", item, "", strINIPath));
                        }

                        listKey_lastdmg.AddRange(GetKeys("last_dmg", strINIPath));
                        foreach (var item in listKey_lastdmg)
                        {
                            listAllValue.Add(GetIniFileString("last_dmg", item, "", strINIPath));
                        }
                    }

                    listKey_ipsw.AddRange(GetKeys("ipsw", strINIPath));
                    foreach (var item in listKey_ipsw)
                    {
                        listAllValue.Add(GetIniFileString("ipsw", item, "", strINIPath));
                    }

                    listKey_dmg.AddRange(GetKeys("dmg", strINIPath));
                    foreach (var item in listKey_dmg)
                    {
                        listAllValue.Add(GetIniFileString("dmg", item, "", strINIPath));
                    }
                    foreach (var item in System.IO.Directory.GetFileSystemEntries(strTargetFolderPath))
                    {
                        listAllLocalFiles.Add(System.IO.Path.GetFileName(item));
                    }

                    drawLoading();
                    gridRing.Visibility = System.Windows.Visibility.Hidden;

                    var listOld = listAllLocalFiles.Except(listAllValue);

                    foreach (var item in listAllLocalFiles)
	                {
                        LogIt(item);
	                }
                    

                    listboxOld.ItemsSource = listOld;

                    tblockCount.Text = LoadResourceString.Instance.GetStrByResource("file_to_remove") + " : " + listOld.Count();

                    if (listOld.Count() == 0)
                    {
                        DeleteButtonGray();
                        if (bFirstCheck)
                        {
                            MessageBox.Show(LoadResourceString.Instance.GetStrByResource("no_old_file"), LoadResourceString.Instance.GetStrByResource("remind"), MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        }
                    }
                    else
                    {
                        bFirstCheck = false;
                        DeleteButtonNormal();
                    }
                }
                else
                {
                    MessageBox.Show("System not found \"iOSFile.ini\". ", LoadResourceString.Instance.GetStrByResource("remind"), MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    this.Close();
                    DeleteButtonGray();
                }
                //var listNew = listAllLocalFiles.Intersect(listAllValue);
                //listboxNew.ItemsSource = listNew;

                //var listLost = listAllValue.Except(listAllLocalFiles);
                //listboxLost.ItemsSource = listLost;

                //if (listLost.Count() == 0)
                //{
                //    listboxLost.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                //}
                //else
                //{
                //    listboxLost.Background = new SolidColorBrush(Color.FromRgb(247, 135, 135));
                //}
                

            }
            catch (Exception err)
            {
                DeleteButtonGray();
            }
            
        }

        private void btnClose_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {

            if (MessageBox.Show(LoadResourceString.Instance.GetStrByResource("delete_confirm"), LoadResourceString.Instance.GetStrByResource("warning"), MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                //no
            }
            else
            {
                try
                {
                    DeleteButtonGray();
                    gridRing.Visibility = System.Windows.Visibility.Visible;
                    btnClose.Visibility = System.Windows.Visibility.Hidden;
                    var listOld = listAllLocalFiles.Except(listAllValue);
                    foreach (var item in listOld)
                    {
                        if (File.Exists(strTargetFolderPath + item))
                        {
                            File.Delete(strTargetFolderPath + item);
                        }
                    }

                    //Delete "D:\ProgramData\Futuredial\CMC\PST\Apple\Common\dmg\"
                    try
                    {
                        string strTargetFolderPath_D = @"D:\ProgramData\Futuredial\CMC\PST\Apple\Common\dmg\";
                        List<string> listAllLocalFiles_D = new List<string>();
                        foreach (var item in System.IO.Directory.GetFileSystemEntries(strTargetFolderPath_D))
                        {
                            listAllLocalFiles_D.Add(System.IO.Path.GetFileName(item));
                        }

                        var listOld_D = listAllLocalFiles_D.Except(listAllValue);
                        foreach (var item in listOld_D)
                        {
                            if (File.Exists(strTargetFolderPath_D + item))
                            {
                                File.Delete(strTargetFolderPath_D + item);
                            }
                        }
                    }
                    catch (Exception) { }

                    //Delete "E:\ProgramData\Futuredial\CMC\PST\Apple\Common\dmg\"
                    try
                    {
                        string strTargetFolderPath_E = @"E:\ProgramData\Futuredial\CMC\PST\Apple\Common\dmg\";
                        List<string> listAllLocalFiles_E = new List<string>();
                        foreach (var item in System.IO.Directory.GetFileSystemEntries(strTargetFolderPath_E))
                        {
                            listAllLocalFiles_E.Add(System.IO.Path.GetFileName(item));
                        }

                        var listOld_E = listAllLocalFiles_E.Except(listAllValue);
                        foreach (var item in listOld_E)
                        {
                            if (File.Exists(strTargetFolderPath_E + item))
                            {
                                File.Delete(strTargetFolderPath_E + item);
                            }
                        }
                    }
                    catch (Exception) { }
                    
                    listAllLocalFiles = new List<string>();
                    foreach (var item in System.IO.Directory.GetFileSystemEntries(strTargetFolderPath))
                    {
                        listAllLocalFiles.Add(System.IO.Path.GetFileName(item));
                    }

                    var listOld2 = listAllLocalFiles.Except(listAllValue);
                    listboxOld.ItemsSource = null;
                    listboxOld.ItemsSource = listOld2;

                    if (listOld2.Count() == 0)
                    {
                        DeleteButtonGray();
                        MessageBox.Show(LoadResourceString.Instance.GetStrByResource("delete_complete"), LoadResourceString.Instance.GetStrByResource("success"), MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    else
                        DeleteButtonNormal();

                    tblockCount.Text = LoadResourceString.Instance.GetStrByResource("file_to_remove") + " : " + listOld2.Count();

                    gridRing.Visibility = System.Windows.Visibility.Hidden;
                    btnClose.Visibility = System.Windows.Visibility.Visible;
                }
                catch (Exception err){
                    gridRing.Visibility = System.Windows.Visibility.Hidden;
                    btnClose.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }

        void drawLoading()
        {
            for (int i = 0; i < 12; i++)
            {
                Line line = new Line()
                {
                    X1 = 25,
                    X2 = 25,
                    Y1 = 0,
                    Y2 = 15,
                    StrokeThickness = 4,
                    Stroke = Brushes.White,
                    Width = 50,
                    Height = 50
                };
                line.VerticalAlignment = VerticalAlignment.Center;
                line.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                line.RenderTransformOrigin = new Point(.5, .5);
                line.RenderTransform = new RotateTransform(i * 30);
                line.Opacity = (double)i / 12;

                canvasLoading.Children.Add(line);
            }

            DoubleAnimation a = new DoubleAnimation();
            a.From = 0;
            a.To = 360;
            a.RepeatBehavior = RepeatBehavior.Forever;
            a.SpeedRatio = 0.5;

            spin.BeginAnimation(RotateTransform.AngleProperty, a);
        }

        void DeleteButtonGray()
        {
            btnDelete.Visibility = System.Windows.Visibility.Hidden;
        }

        void DeleteButtonNormal()
        {
            btnDelete.Visibility = System.Windows.Visibility.Visible;
        }

        public static void LogIt(string s)
        {
            System.Console.Out.WriteLine(s);
            System.Diagnostics.Trace.WriteLine(s);
        }

        private void gridHead_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
