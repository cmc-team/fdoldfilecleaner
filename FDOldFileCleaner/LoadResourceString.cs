﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Resources;
using System.IO;
using System.Globalization;

namespace FDOldFileCleaner
{
    public class LoadResourceString
    {
        public static ResourceDictionary resource;
        public ResourceManager resMgr;
        private static volatile LoadResourceString _instance;
        private Dictionary<string, string> dictionary;
        private string culture=string.Empty;

        /*List<string> supportedLanguage = new List<string>()
	    {
	        "en-US",
	        "ja-JP",
	        "es-ES",
            "zh-CN",
            "zh-TW",
            "ko-KR"
	    };*/

        public string GetStrByResource(string key)
        {
            #region get the value from the existed the dictionary
            if (dictionary == null)
                dictionary = new Dictionary<string, string>();
            if (dictionary.ContainsKey(key))
                return dictionary[key];
            #endregion

            if (string.IsNullOrEmpty(culture))
            {
                CultureInfo cultureInfo = CultureInfo.CurrentCulture;
                culture = cultureInfo.Name;
            }

            /*if (!supportedLanguage.Contains(culture))
                culture = "en-US";*/

            if (resMgr == null)
            {
                try
                {
                    string resourceFile = Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "Languages", string.Format("resource.{0}.{1}.resources", System.Diagnostics.Process.GetCurrentProcess().ProcessName, culture));
                    if (!File.Exists(resourceFile))
                    {
                        culture = "en-US";
                    }
                    resMgr = ResourceManager.CreateFileBasedResourceManager("resource." + System.Diagnostics.Process.GetCurrentProcess().ProcessName + "." + culture, System.IO.Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "Languages"), null);
                }
                catch (Exception)
                {
                    return key;
                }
            }

            try
            {
                string value = resMgr.GetString(key);
                if (string.IsNullOrEmpty(value))
                    return key;

                dictionary.Add(key, value);
                return value;
            }
            catch (Exception) 
            { 
                return key; 
            }            
        }
     
        public static LoadResourceString Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new LoadResourceString();
                return _instance;
            }
        }
    }
}
